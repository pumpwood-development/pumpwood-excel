VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PumpwoodAuthenticator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Implements IWebAuthenticator

Public headerCSRF As String
Public headerToken As String
Dim pumpwoodClient As WebClient

Public Function SetClient(client As WebClient)
    Set pumpwoodClient = client
End Function

Public Function Login(username As String, password As String, url As String) As Boolean
    Dim authClient As New WebClient
    authClient.Insecure = True
    authClient.BaseUrl = url
    Set authClient.Authenticator = Nothing
    
    Dim authRequest As New WebRequest
    authRequest.resource = "registration/login/"
    authRequest.Format = WebFormat.JSON
    authRequest.Method = webMethod.HttpPost
    
    Dim body As New Dictionary
    body.Add "username", username
    body.Add "password", password
    Set authRequest.body = body
    
    Set authResponse = authClient.Execute(authRequest)
    
    Set responseDict = ParseJson(authResponse.Content)
    Me.headerToken = responseDict("token")
    Me.headerCSRF = responseDict("csrf_token")
    
    pumpwoodClient.BaseUrl = url
    Set pumpwoodClient.Authenticator = Me
End Function

Public Function Logout()
    If Not pumpwoodClient Is Nothing And Not pumpwoodClient.Authenticator Is Nothing Then
        Dim req As New WebRequest
        req.resource = "registration/logout/"
        req.Format = WebFormat.JSON
        req.Method = webMethod.HttpGet
        
        Set authResponse = pumpwoodClient.Execute(req)
        
        pumpwoodClient.BaseUrl = Empty
        Set pumpwoodClient.Authenticator = Nothing
        
        Me.headerToken = Empty
        Me.headerCSRF = Empty
    End If
End Function

Private Sub IWebAuthenticator_BeforeExecute(ByVal client As WebClient, ByRef request As WebRequest)
    request.SetHeader "Authorization", "Token " & Me.headerToken
    request.SetHeader "X-CSRFToken", Me.headerCSRF
End Sub

Private Sub IWebAuthenticator_AfterExecute(ByVal client As WebClient, ByVal request As WebRequest, ByRef response As WebResponse)
    ' Me.headerCSRF = Response.Headers("csrf_token")
End Sub

Private Sub IWebAuthenticator_PrepareHttp(ByVal client As WebClient, ByVal request As WebRequest, ByRef Http As Object)
    ' e.g. Update option, headers, etc.
End Sub

Private Sub IWebAuthenticator_PrepareCurl(ByVal client As WebClient, ByVal request As WebRequest, ByRef Curl As String)
    ' e.g. Add flags to cURL
End Sub
