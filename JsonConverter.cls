VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "JsonConverter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim sheet As Worksheet
Dim currentRowIndex As Integer

' arranjar um nome melhor pra classe e pras fun��es

Public Function FromBaseJson(sheetToPopulate As Worksheet, ByVal jsonObj As Object, rowIndex As Integer, columnIndex As Integer) As Object
    Set sheet = sheetToPopulate
    currentRowIndex = rowIndex
    Set FromBaseJson = ReadItem(jsonObj, columnIndex)
End Function

Private Function ReadDictionary(ByVal dict As Dictionary, columnIndex As Integer) As Dictionary
    Dim newDict As New Dictionary
    For Each key In dict.keys
        newDict.Add key, ReadItem(dict(key), columnIndex + 1)
    Next
    Set ReadDictionary = newDict
End Function

Private Function ReadList(ByVal List As Collection, columnIndex As Integer) As Collection
    Dim i As Integer
    Dim newList As New Collection
    For Each item In List
        newList.Add ReadItem(item, columnIndex)
    Next
    Set ReadList = newList
End Function

Private Function ReadItem(item As Variant, columnIndex As Integer) As Variant
    If TypeOf item Is Dictionary Then
        Set ReadItem = ReadDictionary(item, columnIndex)
    ElseIf TypeOf item Is Collection Then
        Set ReadItem = ReadList(item, columnIndex)
    Else
        ReadItem = sheet.Cells(currentRowIndex, columnIndex)
        currentRowIndex = currentRowIndex + 1
    End If
End Function
