VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DataPresenter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Enum DataPresentationType
    Editable
    Locked
    DropDownListTuple
    DropDownListObjectDescription
    Table
End Enum

Dim jsonObj As Object
Dim keys As New Collection
Dim ranges As New Dictionary
Dim types As New Dictionary

Dim optionsObj As Object

Public Function ShowJsonObject(ByVal jsonObject As Object)
    Set jsonObj = jsonObject
    For Each key In keys
        Dim rangeToPresent As Range
        Set rangeToPresent = ranges(key)
        rangeToPresent.Locked = False
        Select Case types(key)
            Case DataPresentationType.Locked
                rangeToPresent.value = jsonObj(key)
                rangeToPresent.Locked = True
            Case DataPresentationType.Editable
                rangeToPresent.value = jsonObj(key)
            Case DataPresentationType.DropDownListTuple
                rangeToPresent.value = jsonObj(key)(2)
            Case DataPresentationType.DropDownListObjectDescription
                rangeToPresent.value = jsonObj(key)("description")
            Case DataPresentationType.Table
                ShowTable jsonObj(key), rangeToPresent
        End Select
    Next
End Function

Public Function InitializeOptions(ByVal optionsObject As Object)
    Set optionsObj = optionsObject
    For Each key In keys
        Dim rangeToPresent As Range
        Dim collectionObj As Collection
        Dim items() As String
        Dim count As Integer
        
        Set rangeToPresent = ranges(key)
        rangeToPresent.Locked = False
        Select Case types(key)
            Case DataPresentationType.DropDownListTuple
                Set collectionObj = optionsObject(key)
                count = collectionObj.count
                ReDim items(count) As String
                For i = 1 To count
                    items(i) = collectionObj(i)(2)
                Next i
                CreateDropDown ranges(key), items
            Case DataPresentationType.DropDownListObjectDescription
                Set collectionObj = optionsObject(key)
                count = collectionObj.count
                ReDim items(count) As String
                For i = 1 To count
                    items(i) = collectionObj(i)("description")
                Next i
                CreateDropDown ranges(key), items
        End Select
    Next
End Function

Private Sub CreateDropDown(dropDownRange As Range, items() As String)
    With dropDownRange.Validation
        .Delete
        .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, _
        Operator:=xlBetween, Formula1:=Join(items, ",")
        .IgnoreBlank = True
        .InCellDropdown = True
        .InputTitle = ""
        .ErrorTitle = ""
        .InputMessage = ""
        .ErrorMessage = ""
        .ShowInput = True
        .ShowError = True
    End With
End Sub


Public Function ShowTable(dict As Dictionary, rangeToPresent As Range)
    Dim currentRowIndex As Integer
    Dim columnIndex As Integer
    Dim totalColumns As Integer
    
    currentRowIndex = rangeToPresent.row
    columnIndex = rangeToPresent.column
    totalColumns = rangeToPresent.Columns.count
    
    Dim valuesToShow() As String
    ReDim valuesToShow(totalColumns - 1)
    
    For i = 1 To totalColumns - 1
        valuesToShow(i) = Cells(currentRowIndex, columnIndex + i)
    Next i
    
    Dim valuesToShowCount As Integer
    valuesToShowCount = UBound(valuesToShow)
    currentRowIndex = currentRowIndex + 1
    
    For itemIndex = 0 To dict.count - 1
        Cells(currentRowIndex + itemIndex, columnIndex) = dict.keys(itemIndex)
        For valueIndex = 1 To valuesToShowCount
            Cells(currentRowIndex + itemIndex, columnIndex + valueIndex) = dict(dict.keys(itemIndex))(valuesToShow(valueIndex))
        Next valueIndex
    Next itemIndex
End Function

Public Function ParseToJsonObject(ByVal baseJsonObject As Object) As Object
    For Each key In keys
        Dim presentedRange As Range
        Set presentedRange = ranges(key)
        Select Case types(key)
            Case DataPresentationType.Locked
                presentedRange.value = jsonObj(key)
            Case DataPresentationType.Editable
                jsonObj(key) = presentedRange.value
            Case DataPresentationType.DropDownListTuple
                Set jsonObj(key) = GetOptionTupleItem(key, presentedRange.value)
            Case DataPresentationType.DropDownListObjectDescription
                Set jsonObj(key) = GetOptionObjectDescriptionItem(key, presentedRange.value)
        End Select
    Next
    Set ParseToJsonObject = jsonObj
End Function

Private Function GetOptionTupleItem(ByVal key As String, value As String) As Object
    For Each item In optionsObj(key)
        If item(2) = value Then
            Set GetOptionTupleItem = item
        End If
    Next
End Function

Private Function GetOptionObjectDescriptionItem(ByVal key As String, value As String) As Object
    For Each item In optionsObj(key)
        If item("description") = value Then
            Set GetOptionObjectDescriptionItem = item
        End If
    Next
End Function

Public Function RegisterField(key As String, Range As Range, presentationType As DataPresentationType)
    keys.Add key
    ranges.Add key, Range
    types.Add key, presentationType
End Function


