VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ListClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim pumpwoodClient As webClient
Private modelClassName As String

Public Function Setup(client As webClient, modelClass As String)
    Set pumpwoodClient = client
    modelClassName = modelClass
End Function

Public Function List(query As Dictionary) As Object
    Dim body As New Dictionary
    body.Add "filter_dict", query
    body.Add "exclude_dict", New Dictionary
    body.Add "ordering_list", Array("id")
    
    Set List = RunPostRequest(modelClassName, "list", body)
End Function

Public Function ListAll(query As Dictionary) As Object
    Dim body As New Dictionary
    body.Add "filter_dict", query
    body.Add "exclude_dict", New Dictionary
    body.Add "ordering_list", Array("id")
    
    Set ListAll = RunPostRequest(modelClassName, "list-without-pag", body)
End Function

Public Function GetSearchOptions()
    Set GetSearchOptions = RunGetRequest(modelClassName, "options")
End Function
