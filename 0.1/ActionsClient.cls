VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ActionsClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim pumpwoodClient As webClient
Private modelClassName As String

Public Function Setup(client As webClient, modelClass As String)
    Set pumpwoodClient = client
    modelClassName = modelClass
End Function

Public Function List() As Object
    Set List = RunGetRequest(modelClassName, "actions")
End Function

Public Function Run(actionName As String, pk As Integer, params As Dictionary) As Object
    Dim functionName As String
    functionName = "actions/" & actionName & "/" & CStr(pk)
    Set Run = RunPostRequest(modelClassName, functionName, params)
End Function

