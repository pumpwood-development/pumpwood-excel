VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RetrieveClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim pumpwoodClient As webClient
Private modelClassName As String

Public Function Setup(client As webClient, modelClass As String)
    Set pumpwoodClient = client
    modelClassName = modelClass
End Function

Public Function Retrieve(pk As Integer) As Object
    Set Retrieve = RunGetRequest(modelClassName, "retrieve/" & CStr(pk))
End Function

Public Function GetOptions(updateData As Dictionary) As Object
    Set GetOptions = RunPostRequest(modelClassName, "options", updateData)
End Function

Public Function Save(updateData As Variant) As Object
    Set Save = RunPostRequest(modelClassName, "save", updateData)
End Function
