Attribute VB_Name = "Helpers"
Public Function ClearColumns(sheet As Worksheet, fromIndex As Integer, toIndex As Integer)
    For i = fromIndex To toIndex
        sheet.Columns(i).ClearContents
    Next i
End Function

Public Function GetIntArray(text As String) As Integer()
    Dim IdTexts() As String
    
    IdTexts() = Split(text, ",")
    ReDim ids(UBound(IdTexts)) As Integer
    For i = 0 To UBound(IdTexts)
        ids(i) = CInt(IdTexts(i))
    Next i
    GetIntArray = ids
End Function

Public Function FixJsonText(jsonText As String) As String
    FixJsonText = Replace(jsonText, ")]}'," & vbLf, "")
End Function

