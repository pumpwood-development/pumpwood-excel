Attribute VB_Name = "Variables"
Dim mainClient As New webClient
Dim pumpAuthenticator As New PumpwoodAuthenticator

Property Get MainPumpwoodClient() As webClient
    Set MainPumpwoodClient = mainClient
End Property

Property Get Authenticator() As PumpwoodAuthenticator
    Set Authenticator = pumpAuthenticator
End Property
