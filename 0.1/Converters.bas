Attribute VB_Name = "Converters"
Public Function FromTableToDictionary(sheet As Worksheet, rowIndex As Integer, columnIndex As Integer) As Dictionary
    Dim currentRow As Integer
    currentRow = rowIndex
    Dim dict As New Dictionary
    Do While Not IsEmpty(sheet.Cells(currentRow, columnIndex))
        If Not IsEmpty(sheet.Cells(currentRow, columnIndex + 1)) Then
            dict.Add sheet.Cells(currentRow, columnIndex), sheet.Cells(currentRow, columnIndex + 1)
        End If
        currentRow = currentRow + 1
    Loop
    Set FromTableToDictionary = dict
End Function

Public Function FromPivotToTable(sheet As Worksheet, rowIndex As Integer, columnIndex As Integer, ByVal dict As Dictionary)
    If dict Is Nothing Then
        Exit Function
    End If
    For Each key In dict.keys
        sheet.Cells(rowIndex, columnIndex) = key
        For i = 1 To dict(key).Count
            sheet.Cells(rowIndex + i, columnIndex) = dict(key)(i)
        Next i
        columnIndex = columnIndex + 1
    Next
End Function
