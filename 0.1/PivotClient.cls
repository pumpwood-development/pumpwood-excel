VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PivotClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim pumpwoodClient As webClient
Private modelClassName As String

Public Function Setup(client As webClient, modelClass As String)
    Set pumpwoodClient = client
    modelClassName = modelClass
End Function

Public Function Pivot(query As Dictionary, pivotBy As String) As Object
    Dim body As New Dictionary
    body.Add "filter_dict", query
    body.Add "columns", Array(pivotBy)
    body.Add "exclude_dict", New Dictionary
    body.Add "ordering_list", Array("id")
    
    Set Pivot = RunPostRequest(modelClassName, "pivot", body)
End Function

Public Function PivotModelQueue(pk As Integer) As Object
    Set PivotModelQueue = RunPostRequest("modelqueue/actions/pivot_model_queue_data", CStr(pk))
End Function
