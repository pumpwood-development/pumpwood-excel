VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PivotPresenter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim jsonObj As Dictionary

Dim startingPoint As Range
Dim baseKeys As Variant
Dim pivotClients As New Dictionary

Dim columnIndex As Integer
Dim rowIndex As Integer

Dim pivotedByName As String

Dim sheet As Worksheet

Public Function SetUp(sheetToPopulate As Worksheet, startingPointRange As Range, baseKeysToShow As Variant)
    Set sheet = sheetToPopulate
    Set startingPoint = startingPointRange
    baseKeys = baseKeysToShow
End Function

Public Function ShowJsonObject(ByVal jsonObject As Dictionary, pivotedBy As String)
    If jsonObject Is Nothing Then
        Exit Function
    End If
    
    Set jsonObj = jsonObject
    columnIndex = startingPoint.column
    rowIndex = startingPoint.row
    pivotedByName = pivotedBy
    
    ResolveHeaders jsonObj.keys
    
    For Each key In baseKeys
        If IsInArray(key, jsonObj.keys) Then
            sheet.Cells(rowIndex, columnIndex) = key
            If IsInArray(key, pivotClients.keys) Then
                sheet.Cells(rowIndex, columnIndex).ClearComments
                sheet.Cells(rowIndex, columnIndex) = key
                FillColumnOfDescriptions key
                jsonObj.Remove key
            Else
                sheet.Cells(rowIndex, columnIndex).ClearComments
                sheet.Cells(rowIndex, columnIndex) = key
                FillColumn key
                jsonObj.Remove key
            End If
        End If
    Next
    
    ' sort remaining keys
    
    For Each key In jsonObj.keys
        If IsNumeric(key) Then
            ShowHeader key
            FillColumn key
        End If
    Next
End Function

Private Function ResolveHeaders(keys As Variant)
    For Each key In baseKeys
        If IsInArray(key, keys) Then
            sheet.Cells(rowIndex, columnIndex) = key
        End If
    Next
    Dim pksToList As New Collection
    For Each key In keys
        If Not IsInArray(key, baseKeys) And IsNumeric(key) Then
            pksToList.Add CInt(key)
        End If
    Next
    If pksToList.count = 0 Then
        Exit Function
    End If
    Dim query As New Dictionary
    query.Add "id__in", pksToList
    Set resultList = pivotClients(pivotedByName).List(query)
    For i = 1 To resultList.count
        sheet.Cells(rowIndex, columnIndex + i - 1) = resultList(i)("description")
        ShowComment rowIndex, columnIndex + i - 1, resultList(i)
    Next i
End Function

Private Function ShowHeader(key)
    Set retrievedData = pivotClients(pivotedByName).Retrieve(CInt(key))
    sheet.Cells(rowIndex, columnIndex) = retrievedData("description")
    ShowComment rowIndex, columnIndex, retrievedData
End Function

Private Function ShowComment(row, column, retrievedData)
    sheet.Cells(row, column).ClearComments
    sheet.Cells(row, column).AddComment "model_class: " & retrievedData("model_class") & vbCrLf & "pk: " & retrievedData("pk") & vbCrLf & "description: " & retrievedData("description") & vbCrLf & "notes: " & retrievedData("notes") & vbCrLf
End Function

Private Function FillColumn(key)
    For i = 1 To jsonObj(key).count
        sheet.Cells(rowIndex + i, columnIndex).ClearComments
        sheet.Cells(rowIndex + i, columnIndex) = jsonObj(key)(i)
    Next i
    columnIndex = columnIndex + 1
End Function

Private Function FillColumnOfDescriptions(key)
    Dim descriptions As New Dictionary
    Dim pks As New Collection
    For Each pk In jsonObj(key)
        If Not descriptions.Exists(pk) Then
            descriptions.Add pk, Nothing
            pks.Add CInt(pk)
        End If
    Next
    Dim query As New Dictionary
    query.Add "id__in", pks
    Set resultList = pivotClients(key).List(query)
    For Each item In resultList
        Set descriptions(item("pk")) = item
    Next
    For i = 1 To jsonObj(key).count
        sheet.Cells(rowIndex + i, columnIndex) = descriptions(jsonObj(key)(i))("description")
        ShowComment rowIndex + i, columnIndex, descriptions(jsonObj(key)(i))
    Next i
    columnIndex = columnIndex + 1
End Function

Public Function RegisterPivotType(key As String, client As pumpwoodClient)
    pivotClients.Add key, client
End Function
