Attribute VB_Name = "Helpers"
Function IsInArray(stringToBeFound, arr As Variant) As Boolean
  IsInArray = (UBound(Filter(arr, stringToBeFound)) > -1)
End Function

Public Function ClearColumns(sheet As Worksheet, fromIndex As Integer, toIndex As Integer)
    For i = fromIndex To toIndex
        sheet.Columns(i).ClearContents
    Next i
End Function

Public Function GetIntArray(text As String) As Integer()
    Dim IdTexts() As String
    
    IdTexts() = Split(text, ",")
    ReDim ids(UBound(IdTexts)) As Integer
    For i = 0 To UBound(IdTexts)
        ids(i) = CInt(IdTexts(i))
    Next i
    GetIntArray = ids
End Function

Public Function Alphabetically_SortArray()
'PURPOSE: Sort an array filled with strings alphabetically (A-Z)
'SOURCE: www.TheSpreadsheetGuru.com/the-code-vault

Dim myArray As Variant
Dim x As Long, y As Long
Dim TempTxt1 As String
Dim TempTxt2 As String

myArray = Array("D", "1", "A", "B", "F", "C", "E")

'Alphabetize Sheet Names in Array List
  For x = LBound(myArray) To UBound(myArray)
    For y = x To UBound(myArray)
      If UCase(myArray(y)) < UCase(myArray(x)) Then
        TempTxt1 = myArray(x)
        TempTxt2 = myArray(y)
        myArray(x) = TempTxt2
        myArray(y) = TempTxt1
      End If
     Next y
  Next x

End Function

Sub ReverseAlphabetical_SortArray()
'PURPOSE: Sort an array filled with strings in reverse alphabetical order (Z-A)
'SOURCE: www.TheSpreadsheetGuru.com/the-code-vault

Dim myArray As Variant
Dim x As Long, y As Long
Dim TempTxt1 As String
Dim TempTxt2 As String

myArray = Array("D", "1", "A", "B", "F", "C", "E")

'Alphabetize Sheet Names in Array List
  For x = LBound(myArray) To UBound(myArray)
    For y = x To UBound(myArray)
      If UCase(myArray(y)) > UCase(myArray(x)) Then
        TempTxt1 = myArray(x)
        TempTxt2 = myArray(y)
        myArray(x) = TempTxt2
        myArray(y) = TempTxt1
      End If
     Next y
  Next x

End Sub

 Function SheetExists(shtName As String, Optional wb As Workbook) As Boolean
    Dim sht As Worksheet

     If wb Is Nothing Then Set wb = ThisWorkbook
     On Error Resume Next
     Set sht = wb.Sheets(shtName)
     On Error GoTo 0
     SheetExists = Not sht Is Nothing
 End Function
