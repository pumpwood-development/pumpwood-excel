VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PumpwoodClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim pumpwoodClient As WebClient
Private modelClassName As String

Public Function SetUp(client As WebClient, modelClass As String)
    Set pumpwoodClient = client
    modelClassName = modelClass
End Function

Public Function ListActions() As Object
    Set ListActions = RunGetRequest(modelClassName, "actions")
End Function

Public Function RunAction(actionName As String, pk As Integer, params As Dictionary) As Object
    Dim functionName As String
    functionName = "actions/" & actionName & "/" & CStr(pk)
    Set RunAction = RunPostRequest(modelClassName, functionName, params)
End Function


Public Function List(query As Dictionary) As Object
    Dim body As New Dictionary
    body.Add "filter_dict", query
    body.Add "exclude_dict", New Dictionary
    body.Add "ordering_list", Array("id")
    
    Set List = RunPostRequest(modelClassName, "list", body)
End Function

Public Function ListAll(query As Dictionary) As Object
    Dim body As New Dictionary
    body.Add "filter_dict", query
    body.Add "exclude_dict", New Dictionary
    body.Add "ordering_list", Array("id")
    
    Set ListAll = RunPostRequest(modelClassName, "list-without-pag", body)
End Function

Public Function GetSearchOptions()
    Set GetSearchOptions = RunGetRequest(modelClassName, "options")
End Function


Public Function Pivot(query As Dictionary, Optional pivotBy As String = Empty) As Object
    Dim body As New Dictionary
    body.Add "filter_dict", query
    If Not IsEmpty(pivotBy) And pivotBy <> "none" Then
        body.Add "columns", Array(pivotBy)
    Else
        body.Add "columns", Array()
    End If
    body.Add "exclude_dict", New Dictionary
    body.Add "ordering_list", Array("id")
    
    Set Pivot = RunPostRequest(modelClassName, "pivot", body)
End Function

Public Function Retrieve(pk As Integer) As Object
    Set Retrieve = RunGetRequest(modelClassName, "retrieve/" & CStr(pk))
End Function

Public Function GetOptions(updateData As Dictionary) As Object
    Set GetOptions = RunPostRequest(modelClassName, "options", updateData)
End Function

Public Function Save(updateData As Variant) As Object
    Set Save = RunPostRequest(modelClassName, "save", updateData)
End Function

Public Function RunPostRequest(modelClassName As String, functionName As String, Optional ByVal body As Variant = Nothing) As Object
    Set RunPostRequest = ExecuteRequest(modelClassName, functionName, webMethod.HttpPost, body)
End Function

Public Function RunGetRequest(modelClassName As String, functionName As String) As Object
    Set RunGetRequest = ExecuteRequest(modelClassName, functionName, webMethod.HttpGet)
End Function

Private Function ExecuteRequest(modelClassName As String, functionName As String, webMethodType As webMethod, Optional ByVal body As Variant = Nothing) As Object
    If MainPumpwoodClient.Authenticator Is Nothing Then
        MsgBox "You are not logged in"
    Else
        Dim req As New WebRequest
        req.resource = "{modelClassName}/{functionName}/"
        req.AddUrlSegment "modelClassName", modelClassName
        req.AddUrlSegment "functionName", functionName
        req.Format = WebFormat.JSON
        req.Method = webMethodType
        req.Accept = "application/json;charset=utf-8"
        
        If Not body Is Nothing Then
            Set req.body = body
        End If
    
        Set response = MainPumpwoodClient.Execute(req)
        Set ExecuteRequest = JSON.parse(response.Content)
    End If
End Function

