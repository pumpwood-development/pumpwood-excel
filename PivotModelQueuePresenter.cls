VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PivotModelQueuePresenter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim columnIndex As Integer

Public Function ShowPivotedValues(sheet As Worksheet, rangeToShow As Range, pk As Integer)
    Set jsonObj = ModelQueueClient.RunAction("pivot_model_queue_data", pk, Nothing)
    If Not jsonObj.Exists("result") Then
        MsgBox "Pivot not found"
        Exit Function
    End If
    Set jsonObj = jsonObj("result")

    Dim rowIndex As Integer
    columnIndex = rangeToShow.column
    rowIndex = rangeToShow.row
    
    Dim mainHeaders As Variant
    mainHeaders = Array("index", "time", "geoarea_id", "modeling_unit_id")
    Set mainPresenter = New pivotPresenter
    mainPresenter.SetUp sheet, sheet.Cells(rowIndex, columnIndex), mainHeaders
    mainPresenter.RegisterPivotType "modeling_unit_id", DescriptionModelingUnitClient
    mainPresenter.RegisterPivotType "geoarea_id", GeoareaClient
    mainPresenter.ShowJsonObject GetMainColumns(jsonObj, mainHeaders), Empty
    
    Set attributePresenter = New pivotPresenter
    attributePresenter.SetUp sheet, sheet.Cells(rowIndex, columnIndex), Array()
    attributePresenter.RegisterPivotType "attribute", DescriptionAttributeClient
    attributePresenter.ShowJsonObject GetDictionaryWithPrefixes(jsonObj, "ATT"), "attribute"

    Set calendarPresenter = New pivotPresenter
    calendarPresenter.SetUp sheet, sheet.Cells(rowIndex, columnIndex), Array()
    calendarPresenter.RegisterPivotType "calendar", DescriptionCalVariableClient
    calendarPresenter.ShowJsonObject GetDictionaryWithPrefixes(jsonObj, "CAL"), "calendar"
    
    Set descriptionGeoPresenter = New pivotPresenter
    descriptionGeoPresenter.SetUp sheet, sheet.Cells(rowIndex, columnIndex), Array()
    descriptionGeoPresenter.RegisterPivotType "geodescriptionvariable", GeoDescriptionVariableClient
    descriptionGeoPresenter.ShowJsonObject GetDictionaryWithPrefixes(jsonObj, "GEO"), "geodescriptionvariable"
    
    Set filterPresenter = New pivotPresenter
    filterPresenter.SetUp sheet, sheet.Cells(rowIndex, columnIndex), Array()
    filterPresenter.RegisterPivotType "descriptionfilter", DescriptionFilterClient
    filterPresenter.ShowJsonObject GetDictionaryWithPrefixes(jsonObj, "FIL"), "descriptionfilter"
End Function

Public Function GetMainColumns(ByVal jsonObj As Dictionary, keys As Variant) As Dictionary
    Dim dict As New Dictionary
    For Each key In keys
        dict.Add key, jsonObj(key)
        columnIndex = columnIndex + 1
    Next
    Set GetMainColumns = dict
End Function

Public Function GetDictionaryWithPrefixes(ByVal jsonObj As Dictionary, prefix As String) As Dictionary
    Dim pk As String
    Dim dict As New Dictionary
    For Each key In jsonObj.keys
        If InStr(key, prefix) Then
            pk = Split(key, "_")(1)
            dict.Add pk, jsonObj(key)
            columnIndex = columnIndex + 1
        End If
    Next
    Set GetDictionaryWithPrefixes = dict
End Function
