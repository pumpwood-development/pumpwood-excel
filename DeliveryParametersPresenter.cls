VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DeliveryParametersPresenter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim currentRow As Integer
Dim currentColumn As Integer
Dim headers As Variant

Public Function ShowParametersFromDelivery(deliveryPk As Integer, baseRange As Range)
    Set jsonObj = DeliveryBasketClient.Retrieve(deliveryPk)
    If Not jsonObj.Exists("delivery") Then
        MsgBox "Delivery not found"
        Exit Function
    End If
    Set deliveries = jsonObj("delivery")

    headers = Array("parameter", "type", "field description", "result type", "value")
    
    currentColumn = baseRange.column
    
    For Each deliveryInfo In deliveries
        currentRow = baseRange.row
        Set modelQueue = ModelQueueClient.Retrieve(CInt(deliveryInfo("model_queue")))
        ShowModelQueueInfo modelQueue
        ShowHeaders
        WriteDescriptionParameters modelQueue, "attribute"
        WriteDescriptionParameters modelQueue, "filter"
        WriteDescriptionParameters modelQueue, "autofilter"
        WriteDescriptionParameters modelQueue, "calendar"
        WriteDescriptionParameters modelQueue, "geography"
        WriteParameters modelQueue
        currentColumn = currentColumn + UBound(headers) + 2
    Next
End Function

Private Function ShowModelQueueInfo(jsonObj)
    WriteInfo "pk", jsonObj("pk")
    WriteInfo "model", jsonObj("model")("description")
    WriteDescription currentRow, currentColumn, jsonObj("model")
    WriteInfo "created_at", jsonObj("created_at")
    WriteInfo "sent_by", jsonObj("sent_by")("username")
    WriteInfo "", ""
End Function

Private Function WriteInfo(key As String, value)
    Cells(currentRow, currentColumn) = key
    Cells(currentRow, currentColumn + 1) = value
    currentRow = currentRow + 1
End Function

Private Function ShowHeaders()
    For i = 0 To UBound(headers)
        Cells(currentRow, currentColumn + i) = headers(i)
    Next i
    currentRow = currentRow + 1
End Function

Private Function WriteParameters(jsonObj)
    For Each par In jsonObj("parameters_results")("parameter")
        WriteRow par
    Next
End Function

Private Function WriteRow(jsonObj)
    For Each par In jsonObj("parameters")
        Set parValues = par("values")
        For Each key In parValues.keys
            Cells(currentRow, currentColumn) = jsonObj("obj")("description")
            Cells(currentRow, currentColumn + 1) = ""
            Cells(currentRow, currentColumn + 2) = ""
            Cells(currentRow, currentColumn + 3) = key
            Cells(currentRow, currentColumn + 4) = parValues(key)
            currentRow = currentRow + 1
        Next
    Next
End Function

Private Function WriteDescriptionParameters(jsonObj, parameterType As String)
    For Each par In jsonObj("parameters_results")(parameterType)
        WriteDescriptionRow par, parameterType
    Next
End Function

Private Function WriteDescriptionRow(jsonObj, parameterType As String)
    For Each par In jsonObj("parameters")
        Set parValues = par("values")
        For Each key In parValues.keys
            Cells(currentRow, currentColumn) = ""
            Cells(currentRow, currentColumn + 1) = parameterType
            WriteDescription currentRow, currentColumn + 2, jsonObj("obj")
            Cells(currentRow, currentColumn + 3) = key
            Cells(currentRow, currentColumn + 4) = parValues(key)
            currentRow = currentRow + 1
        Next
    Next
End Function

Private Function WriteDescription(row, column, retrievedData)
    Cells(row, column).ClearComments
    Cells(row, column) = retrievedData("description")
    Cells(row, column).AddComment "model_class: " & retrievedData("model_class") & vbCrLf & "pk: " & retrievedData("pk") & vbCrLf & "description: " & retrievedData("description") & vbCrLf
End Function

