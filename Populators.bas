Attribute VB_Name = "Populators"

Public Function List30Items(sheet As Worksheet, modelClassName As String, ByVal query As Dictionary)
    Dim listClient As New listClient
    listClient.SetUp MainPumpwoodClient, modelClassName
    Set jsonObj = listClient.List(query)
    
    If jsonObj Is Nothing Then
        Exit Function
    End If
    
    ClearColumns sheet, 5, 11
    
    sheet.Cells(1, 7) = "count"
    sheet.Cells(1, 8) = jsonObj.count
    sheet.Cells(1, 5) = "pk"
    sheet.Cells(1, 6) = "description"
    Dim currentRow As Integer
    currentRow = 2
    Dim item As Variant
    For Each item In jsonObj
        sheet.Cells(currentRow, 5) = item("pk")
        sheet.Cells(currentRow, 6) = item("description")
        currentRow = currentRow + 1
    Next
    Set jsonObj = Nothing
End Function

Public Function ListAllItems(sheet As Worksheet, listClient As pumpwoodClient, ByVal query As Dictionary, rowIndex As Integer, columnIndex As Integer, headers As Variant)

    Set jsonObj = listClient.ListAll(query)
    
    If jsonObj Is Nothing Then
        Exit Function
    End If
    
    For i = 0 To UBound(headers)
        sheet.Cells(rowIndex, columnIndex + i) = headers(i)
    Next i
    
    Dim currentRow As Integer
    currentRow = rowIndex + 1
    Dim item As Variant
    For Each item In jsonObj
        For i = 0 To UBound(headers)
            sheet.Cells(currentRow, columnIndex + i) = item(headers(i))
        Next i
        currentRow = currentRow + 1
    Next
    Set jsonObj = Nothing
End Function

Public Function ListSearchOptions(sheet As Worksheet, modelClassName As String)
    Dim listClient As New listClient
    listClient.SetUp MainPumpwoodClient, modelClassName
    Set jsonObj = listClient.GetSearchOptions
    
    If jsonObj Is Nothing Then
        Exit Function
    End If
    
    ClearColumns sheet, 5, 11
    
    ' sheet.Cells(1, "B") = "pk"
    ' sheet.Cells(1, "C") = "description"
    
    Dim currentBaseColumn As Integer
    Dim item As Variant
    Dim currentRow As Integer
    Dim currentColumnPos As Integer
    
    currentBaseColumn = 12
    For Each key In jsonObj.keys
        If key <> Empty Then
            currentRow = 3
            sheet.Cells(currentRow - 2, currentBaseColumn) = key
            For Each rowkey In jsonObj(key)(1).keys
                sheet.Cells(currentRow - 1, currentBaseColumn + currentColumnPos) = rowkey
                currentColumnPos = currentColumnPos + 1
            Next
            For Each item In jsonObj(key)
                currentColumnPos = 0
                For Each rowkey In item.keys
                    sheet.Cells(currentRow, currentBaseColumn + currentColumnPos) = item(rowkey)
                    currentColumnPos = currentColumnPos + 1
                Next
                currentColumnPos = 0
                currentRow = currentRow + 1
            Next
            currentBaseColumn = currentBaseColumn + UBound(jsonObj(key)(1).keys) + 1
        End If
        
    Next
    Set jsonObj = Nothing
End Function

Public Function RetrieveInfo(sheet As Worksheet, client As pumpwoodClient, pk As Integer, columnIndex As Integer)
    Set jsonObj = client.Retrieve(pk)
    
    Dim formatter As New JsonFormatter
    formatter.Populate sheet, jsonObj, 1, columnIndex
    Set jsonObj = Nothing
End Function

Public Function ListOptions(sheet As Worksheet, modelClassName As String, columnIndex As Integer)
    Dim RetrieveClient As New RetrieveClient
    RetrieveClient.SetUp MainPumpwoodClient, modelClassName
    Set jsonObj = RetrieveClient.GetOptions(New Dictionary)
    
    Dim formatter As New JsonFormatter
    formatter.Populate sheet, jsonObj, 1, columnIndex
    Set jsonObj = Nothing
End Function

Public Function SaveInfo(sheet As Worksheet, client As pumpwoodClient, pk As Integer, columnIndex As Integer)
    Set jsonObj = client.Retrieve(pk)

    Dim converter As New JsonConverter
    Set jsonObjToUpdate = converter.FromBaseJson(sheet, jsonObj, 1, columnIndex)
    Set jsonObj = client.Save(jsonObjToUpdate)

    Dim formatter As New JsonFormatter
    formatter.Populate sheet, jsonObj, 1, columnIndex
    Set jsonObj = Nothing
End Function

Public Function ListActions(sheet As Worksheet, modelClassName As String, columnIndex As Integer)
    Dim actions As New ActionsClient
    actions.SetUp MainPumpwoodClient, modelClassName
    Set jsonObj = actions.List
    
    Dim formatter As New JsonFormatter
    formatter.Populate sheet, jsonObj, 1, columnIndex
    Set jsonObj = Nothing
End Function

Public Function RunAction(sheet As Worksheet, modelClassName As String, actionName As String, pk As Integer, params As Dictionary, columnIndex As Integer)
    Dim actions As New ActionsClient
    actions.SetUp MainPumpwoodClient, modelClassName
    Set jsonObj = actions.Run(actionName, pk, params)
    
    Dim formatter As New JsonFormatter
    formatter.Populate sheet, jsonObj, 1, columnIndex
    Set jsonObj = Nothing
End Function

Public Function PivotData(sheet As Worksheet, client As pumpwoodClient, query As Dictionary, pivotBy As String, columnIndex As Integer)
    Set jsonObj = client.Pivot(query, pivotBy)
    
    FromPivotToTable sheet, 1, columnIndex, jsonObj
    Set jsonObj = Nothing
End Function

Public Function PivotModelQueue(sheet As Worksheet, pk As Integer, columnIndex As Integer)
    Set jsonObj = DescriptionAttributeClient.PivotModelQueue(pk)
    
    If Not jsonObj Is Nothing Then
        FromPivotToTable sheet, 1, columnIndex, jsonObj("result")
        Set jsonObj = Nothing
    End If
End Function


