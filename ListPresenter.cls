VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ListPresenter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Enum ListPresentationType
    Standard
    Tuple
    ObjectDescription
End Enum

Dim jsonObj As Object
Dim ranges As New Dictionary
Dim types As New Dictionary
Dim keys As New Collection

Dim sheet As Worksheet

Public Function ShowJsonObject(ByVal jsonObj As Object)
    If jsonObj Is Nothing Then
        Exit Function
    End If

    Dim count As Integer
    Dim currentRow As Integer
    Dim currentColumn As Integer
    Dim rangeToPresent As Range
    
    count = jsonObj.count
    
    For Each key In keys
        Set rangeToPresent = ranges(key)
        currentRow = rangeToPresent.row
        currentColumn = rangeToPresent.column
        ' ugly, but fast
        Select Case types(key)
            Case ListPresentationType.Standard
                For i = 1 To count
                    sheet.Cells(currentRow + i, currentColumn) = jsonObj(i)(key)
                Next i
            Case ListPresentationType.Tuple
                For i = 1 To count
                    sheet.Cells(currentRow + i, currentColumn) = jsonObj(i)(key)(2)
                Next i
            Case ListPresentationType.ObjectDescription
                For i = 1 To count
                    If IsNull(jsonObj(i)(key)) Then
                        sheet.Cells(currentRow + i, currentColumn) = Empty
                    Else
                        sheet.Cells(currentRow + i, currentColumn) = jsonObj(i)(key)("description")
                    End If
                Next i
        End Select
    Next
End Function

Public Function SetSheet(sheetToPopulate As Worksheet)
    Set sheet = sheetToPopulate
End Function

Public Function RegisterRange(key As String, rangeToPresent As Range, presentationType As ListPresentationType)
    keys.Add key
    ranges.Add key, rangeToPresent
    types.Add key, presentationType
End Function
