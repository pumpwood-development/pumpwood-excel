VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UserFormPivot 
   Caption         =   "Database Variable Pivot"
   ClientHeight    =   3000
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5730
   OleObjectBlob   =   "UserFormPivot.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "UserFormPivot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim presenter As pivotPresenter
Dim client As pumpwoodClient

Dim type1 As String
Dim type1Name As String

Dim type2 As String
Dim type2Name As String

Dim headers As Variant

Private Sub CommandButtonApply_Click()
    Dim sheet As Worksheet
    Set sheet = GetOrCreateWorksheet()
    
    If TypeName(sheet) <> "Worksheet" Then
        Exit Sub
    End If
    
    Dim query As New Dictionary
    query.Add type1 & "_id__in", GetIntArray(TextBoxPivotType1.text)
    query.Add type2 & "_id__in", GetIntArray(TextBoxPivotType2.text)

    If ComboBoxPivotedBy.text = "None" Then
        ListAllItems sheet, client, query, 1, 1, headers
    Else
        Dim pivotedBy As String
        If ComboBoxPivotedBy.text = type1Name Then
            pivotedBy = type1
        ElseIf ComboBoxPivotedBy.text = type2Name Then
            pivotedBy = type2
        End If
        
        Set jsonObj = client.Pivot(query, pivotedBy)
        presenter.SetUp sheet, sheet.Columns(1), headers
        presenter.ShowJsonObject jsonObj, pivotedBy
    End If
End Sub

Private Sub CommandButtonClose_Click()
    Unload Me
End Sub

Private Function GetOrCreateWorksheet() As Worksheet
    Dim sheet As Worksheet
    Dim sheetName As String
    sheetName = TextBoxWorksheetName.text
    If SheetExists(sheetName) Then
        Dim message As String
        message = "The Worksheet " & """sheetName""" & " already exists. This action is going to clear all data from this worksheet. Do you wish to continue?"
        Dim msgBoxResult As VbMsgBoxResult
        msgBoxResult = MsgBox(message, vbYesNo)
        If msgBoxResult = VbMsgBoxResult.vbYes Then
            Set sheet = Worksheets(sheetName)
            sheet.Cells.Delete
            sheet.UsedRange.ClearComments
        Else
            Set sheet = Nothing
        End If
    Else
        Set sheet = Sheets.Add
        sheet.Name = sheetName
    End If
    Set GetOrCreateWorksheet = sheet
End Function


Private Sub UserForm_Initialize()
    SetDatabaseVariableForm
End Sub

Private Function SetDatabaseVariableForm()
    type1 = "modeling_unit"
    type1Name = "Modeling Unit"
    
    type2 = "attribute"
    type2Name = "Attribute"
    
    LabelPivotType1.Caption = type1Name & "s"
    LabelPivotType2.Caption = type2Name & "s"
    
    With ComboBoxPivotedBy
        .Clear
        .AddItem "None"
        .AddItem type1Name
        .AddItem type2Name
    End With
    
    headers = Array("time", "geoarea", "modeling_unit", "attribute", "value")
    
    Set presenter = New pivotPresenter
    presenter.RegisterPivotType "attribute", DescriptionAttributeClient
    presenter.RegisterPivotType "modeling_unit", DescriptionModelingUnitClient
    presenter.RegisterPivotType "geoarea", GeoareaClient
    
    Set client = DatabaseVariableClient
End Function

Private Function SetGeoDatabaseVariableForm()
    type1 = "geoarea"
    type1Name = "Geography Area"
    
    type2 = "geo_variable"
    type2Name = "Geography Variable"
    
    LabelPivotType1.Caption = type1Name & "s"
    LabelPivotType2.Caption = type2Name & "s"
    
    With ComboBoxPivotedBy
        .Clear
        .AddItem "None"
        .AddItem type1Name
        .AddItem type2Name
    End With
    
    headers = Array("time", "geoarea", "geo_variable", "value")
    
    Set presenter = New pivotPresenter
    presenter.RegisterPivotType "geoarea", GeoareaClient
    presenter.RegisterPivotType "geo_variable", GeoDescriptionVariableClient
    
    Set client = GeoDatabaseVariableClient
End Function
