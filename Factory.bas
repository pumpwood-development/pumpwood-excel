Attribute VB_Name = "Factory"
Dim mainClient As New WebClient
Dim pumpAuthenticator As New PumpwoodAuthenticator

Property Get MainPumpwoodClient() As WebClient
    Set MainPumpwoodClient = mainClient
End Property

Property Get Authenticator() As PumpwoodAuthenticator
    pumpAuthenticator.SetClient mainClient
    Set Authenticator = pumpAuthenticator
End Property

Private Function CreateClient(modelClass As String) As pumpwoodClient
    Dim client As New pumpwoodClient
    mainClient.Insecure = True
    client.SetUp mainClient, modelClass
    Set CreateClient = client
End Function

' database

Property Get DescriptionAttributeClient() As pumpwoodClient
    Set DescriptionAttributeClient = CreateClient("descriptionattribute")
End Property

Property Get DescriptionModelingUnitClient() As pumpwoodClient
    Set DescriptionModelingUnitClient = CreateClient("descriptionmodelingunit")
End Property

Property Get DatabaseVariableClient() As pumpwoodClient
    Set DatabaseVariableClient = CreateClient("databasevariable")
End Property

Property Get DataInputDatabaseVariableClient() As pumpwoodClient
    Set DataInputDatabaseVariableClient = CreateClient("datainputdatabasevariable")
End Property

' calendar

Property Get DescriptionCalVariableClient() As pumpwoodClient
    Set DescriptionCalVariableClient = CreateClient("descriptioncalvariable")
End Property

Property Get CalDatabaseVariableClient() As pumpwoodClient
    Set CalDatabaseVariableClient = CreateClient("caldatabasevariable")
End Property

Property Get DataInputCalendarClient() As pumpwoodClient
    Set DataInputCalendarClient = CreateClient("datainputcalendar")
End Property

' geography

Property Get GeoareaClient() As pumpwoodClient
    Set GeoareaClient = CreateClient("geographyarea")
End Property

Property Get GeoDescriptionVariableClient() As pumpwoodClient
    Set GeoDescriptionVariableClient = CreateClient("geodescriptionvariable")
End Property

Property Get GeoDatabaseVariableClient() As pumpwoodClient
    Set GeoDatabaseVariableClient = CreateClient("geodatabasevariable")
End Property

' estimation

Property Get DescriptionFilterClient() As pumpwoodClient
    Set DescriptionFilterClient = CreateClient("descriptionfilter")
End Property

Property Get FilterClient() As pumpwoodClient
    Set FilterClient = CreateClient("filter")
End Property

Property Get DescriptionModelClient() As pumpwoodClient
    Set DescriptionModelClient = CreateClient("descriptionmodel")
End Property

Property Get ModelQueueClient() As pumpwoodClient
    Set ModelQueueClient = CreateClient("modelqueue")
End Property

' Delivery basket

Property Get DeliveryBasketClient() As pumpwoodClient
    Set DeliveryBasketClient = CreateClient("deliverybasket")
End Property
