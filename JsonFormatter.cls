VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "JsonFormatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim sheet As Worksheet
Dim currentRowIndex As Integer

Public Function Populate(sheetToPopulate As Worksheet, ByVal jsonObj As Object, rowIndex As Integer, columnIndex As Integer)
    If jsonObj Is Nothing Then
        Exit Function
    End If
    Set sheet = sheetToPopulate
    currentRowIndex = rowIndex
    WriteItem jsonObj, columnIndex
End Function

Private Function WriteDictionary(ByVal dict As Dictionary, columnIndex As Integer)
    For Each key In dict.keys
        sheet.Cells(currentRowIndex, columnIndex) = key
        sheet.Cells(currentRowIndex, columnIndex).HorizontalAlignment = xlRight
        WriteItem dict(key), columnIndex + 1
    Next
End Function

Private Function WriteList(ByVal List As Collection, columnIndex As Integer)
    For Each item In List
        WriteItem item, columnIndex
    Next
End Function

Private Function WriteItem(item As Variant, columnIndex As Integer)
    If TypeOf item Is Dictionary Then
        WriteDictionary item, columnIndex
    ElseIf TypeOf item Is Collection Then
        WriteList item, columnIndex
    Else
        sheet.Cells(currentRowIndex, columnIndex) = item
        sheet.Cells(currentRowIndex, columnIndex).HorizontalAlignment = xlLeft
        currentRowIndex = currentRowIndex + 1
    End If
End Function

